<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;
class Products extends Component
{
    use WithPagination;
    public $product_id , $name, $in_date,$out_date,$exp_date,$sku,$buffer_level,$buy_price,$sell_price,$total,
    $brand, $supplier_id,$category_id,$location_id,$description;
    public $isOpen = 0;
    public function render()
    {

        return view('livewire.product.home', [
            'products' => Product::paginate(5),
        ])->layout('layouts.appcontent');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function openModal()
    {
        $this->emit('showModal');
        // $this->isOpen = true;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function closeModal()
    {
        $this->emit('closeModal');

        // $this->isOpen = false;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields(){
        $this->name         = '';
        $this->in_date      = '';
        $this->out_date     = '';
        $this->exp_date     = '';
        $this->sku          = '';
        $this->buffer_level = '';
        $this->buy_price    = '';
        $this->sell_price   = '';
        $this->brand        = '';
        $this->total        = '';
        $this->description  = '';
        $this->location_id  = '';
        $this->category_id  = '';
        $this->supplier_id  = '';
        $this->product_id   = '';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
        $this->validate([
            'name' => 'required',
            'total' => 'required',
        ]);

        Product::updateOrCreate(['id' => $this->product_id], [
            'name' => $this->name,
           /*  'in_date' => $this->in_date,
            'out_date' => $this->out_date,
            'exp_date' => $this->exp_date, */
            'sku' => $this->sku,
            'buffer_level' =>$this->buffer_level,
            'buy_price' =>$this->buy_price,
            'sell_price' =>$this->sell_price,
            'brand' =>$this->brand,
            'total' =>$this->total,
            'description' =>$this->description,
           /*  'category_id' =>$this->category_id,
            'supplier_id' =>$this->supplier_id,
            'location_id' =>$this->location_id, */
        ]);

        session()->flash('message',
            $this->product_id ? 'Product Updated Successfully.' : 'Product Created Successfully.');

        $this->closeModal();
        $this->resetInputFields();
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit($id)
    {
        $post = Product::findOrFail($id);
        $this->product_id   = $id;
        $this->name         = $post->name;
        $this->sku          =  $post->sku;
        $this->buffer_level =  $post->buffer_level;
        $this->buy_price    = $post->buy_price;
        $this->sell_price   = $post->sell_price;
        $this->brand        = $post->brand;
        $this->total        = $post->total;
        $this->description  = $post->description;
        $this->openModal();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete($id)
    {
        Product::find($id)->delete();
        session()->flash('message', 'Post Deleted Successfully.');
    }
}
