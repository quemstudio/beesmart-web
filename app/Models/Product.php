<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected  $fillable = ['products', 'product_id' , 'name', 'in_date','out_date','exp_date',
    'sku','buffer_level','buy_price','sell_price','total',
    'brand','supplier_id','category_id','location_id','description'];
}
