<section class="wrapper site-min-height">
    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <section class="card">
                <header class="card-header">
                    Basic Table
                </header>
                <div class="card-body">
                    <div class="adv-table">
                        <button type="button" class="btn btn-primary float-right mb-1" wire:click="create()">Add Product</button>

                        @include('livewire.product.create')
                        
                        <table class="table  table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th >No.</th>
                                <th >Name</th>
                                <th >Total</th>
                                <th >Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $produt)
                                <tr>
                                    <td >{{ $produt->id }}</td>
                                    <td >{{ $produt->name }}</td>
                                    <td >{{ $produt->total }}</td>
                                    <td >
                                        <button wire:click="edit({{ $produt->id }})" class="btn btn-sm btn-warning">Edit</button>
                                        <button wire:click="delete({{ $produt->id }})" class=" btn btn-sm btn-danger">Delete</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>
<!-- page end-->
@push('scripts')
<script type="text/javascript">
    window.livewire.on('showModal', () => {
        $('#modals').modal('show');
    });
    window.livewire.on('closeModal', () => {
        $('#modals').modal('hide');
    });
</script>
@endpush
