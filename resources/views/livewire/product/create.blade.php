
<!-- Modal Edit -->
<div class="modal fade" id="modals" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="edit-modal-label">Add data
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form_edit" method="post">
            @csrf
            <input name="_method" type="hidden" value="PUT">
                <div class="modal-body">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="product_name" class="form-label right">Name</label>
                                    <input type="text" id="product_name" wire:model="name" class="form-control" required>
                                    @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="product_brand" class="form-label right">Brand</label>
                                    <input type="text" id="product_brand" wire:model="brand" class="form-control" required>
                                    @error('brand') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="product_in_data" class="form-label right">In Date</label>
                                    <input type="text" id="product_in_data" wire:model="in_date" class="form-control" required>
                                    @error('in_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="product_out_date" class="form-label right">Out Date</label>
                                    <input type="text" id="product_out_date" wire:model="out_date" class="form-control" required>
                                    @error('out_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="product_exp_date" class="form-label right">Expired Date</label>
                                    <input type="text" id="product_exp_date" name="name" wire:model="exp_date" class="form-control" required>
                                    @error('exp_date') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="product_sku" class="form-label right">SKU</label>
                                    <input type="text" id="product_sku" wire:model="sku" class="form-control" required>
                                    @error('sku') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="product_buy_price" class="form-label right">Buy Price</label>
                                    <input type="text" id="product_buy_price" wire:model="buy_price" class="form-control" required>
                                    @error('buy_price') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="product_sell_price" class="form-label right">Sell Price</label>
                                    <input type="text" id="product_sell_price" wire:model="sell_price" class="form-control" required>
                                    @error('sell_price') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="product_total" class="form-label right">Total Pcs</label>
                                    <input type="text" id="product_total" wire:model="total" class="form-control" required>
                                    @error('total') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group">
                                    <label for="product_description" class="form-label right">Description</label>
                                    <input type="text" id="product_description" wire:model="description" class="form-control" required>
                                    @error('description') <span class="text-danger">{{ $message }}</span>@enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button wire:click.prevent="store()" type="button" class="btn btn-primary">
                        <span class="fa fa-download mr-1"></span>Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


{{-- <div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

      <div class="fixed inset-0 transition-opacity">
        <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
      </div>

      <!-- This element is to trick the browser into centering the modal contents. -->
      <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>​

      <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
        <form>
        <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
          <div class="">
                <div class="mb-4">
                    <label for="product_name" class="block text-gray-700 text-sm font-bold mb-2">Name:</label>
                    <input type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="product_name" placeholder="Name" name="name" wire:model="name">
                    @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div class="mb-4">
                    <label for="product_brand" class="block text-gray-700 text-sm font-bold mb-2">Brand:</label>
                    <input type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="product_brand" placeholder="Brand" wire:model="brand">
                    @error('brand') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div class="mb-4">
                    <label for="product_sku" class="block text-gray-700 text-sm font-bold mb-2">SKU:</label>
                    <input type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="product_sku" placeholder="Sku" wire:model="sku">
                    @error('sku') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div class="mb-4">
                    <label for="product_buy_price" class="block text-gray-700 text-sm font-bold mb-2">Buy Price:</label>
                    <input type="number" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="product_buy_price" placeholder="Buy Price" wire:model="buy_price">
                    @error('buy_price') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div class="mb-4">
                    <label for="product_sell_price" class="block text-gray-700 text-sm font-bold mb-2">Sell Price:</label>
                    <input type="number" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="product_sell_price" placeholder="Buy Price" wire:model="sell_price">
                    @error('sell_price') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div class="mb-4">
                    <label for="product_total" class="block text-gray-700 text-sm font-bold mb-2">Total:</label>
                    <input type="number" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="product_total" placeholder="Total" wire:model="total">
                    @error('total') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div class="mb-4">
                    <label for="product_description" class="block text-gray-700 text-sm font-bold mb-2">Description:</label>
                    <textarea class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="product_description" wire:model="description" placeholder="Enter Description"></textarea>
                    @error('description') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>

          </div>
        </div>

        <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
          <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
            <button wire:click.prevent="store()" type="button" class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-green-500 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
              Save
            </button>
          </span>
          <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
            <button wire:click="closeModal()" type="button" class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
              Cancel
            </button>
          </span>
        </div>
        </form>

      </div>
    </div>
  </div>
 --}}
